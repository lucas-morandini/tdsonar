package pizzas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class TestPizzas {

	private BasePizzas base=new BasePizzas();
	
	@BeforeEach
	public void init() {
		base.addPizzaToMenu(base.createSurpriseWhitePizza());
	}
	
	@Test
	void testAjoutPizza() {
		Pizza p=new Pizza("fromages", 10);
		p.ajoutIngredient(new Ingredient("Mozzarelle", true));
		p.ajoutIngredient(new Ingredient("Talegio", true));
		
		base.addPizzaToMenu(p);
		assertEquals("fromages",p.getNom());
	}
	
	@Test
	 void testAjoutIng1() {
		Pizza p=base.getPizzaFromMenu("Surprise blanche");
		System.out.println(p.formattedIngredients());
		var oldSize=p.ingredients().length;
		p.ajoutIngredient(new Ingredient("brocolis", true));
		assertEquals(p.ingredients().length,oldSize+1);
	}

	@Test
	void testPizzaMissingIngredients(){
		BasePizzas bp = new BasePizzas();
		Pizza p=new Pizza("tomate",10);
		p.ajoutIngredient(new Ingredient("Tomate",true));
		bp.addPizzaToMenu(p);
		List<Pizza> pizzas = new ArrayList<>();
		pizzas.add(p);

		assertEquals(pizzas,bp.pizzasWithMissingIngredient());;
	}

	@Test
	void testPizza(){
		Pizza pRouge = new Pizza("margarita",10);
		pRouge.ajoutIngredient(new Ingredient("sauce tomate",true));
		Pizza pBlanche = new Pizza("savoyarde",10);
		pBlanche.ajoutIngredient(new Ingredient("crème fraîche",true));
		assertTrue(pBlanche.estBlanche());
		assertFalse(pBlanche.estRouge());
		assertTrue(pRouge.estRouge());
		assertFalse(pRouge.estBlanche());
		assertTrue(pRouge.estVegetarienne());
		assertEquals(10,pRouge.getPrix());
		pBlanche.ajoutIngredient(new Ingredient("jambon",false));
		pBlanche.veganize();
		assertTrue(pBlanche.estVegetarienne());
		assertNotEquals(pBlanche,pRouge);
		Pizza pRouge2 = new Pizza("margarita",10);
		assertEquals(pRouge,pRouge2);
	}

	@Test
	void testFormattedIngredients(){
		Pizza p = new Pizza("pizza",10);
		p.ajoutIngredient(new Ingredient("sauce tomate",true));
		p.ajoutIngredient(new Ingredient("jambon",false));
		assertEquals("sauce tomate jambon ",p.formattedIngredients());
	}

}
